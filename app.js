const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
require('dotenv').config();
const mongoose = require('mongoose');

const indexRouter = require('./routes/index');
const ordersRouter = require('./routes/orders');

const passportJWT = require('./middlewares/passport-jwt');

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//connect to mongodb server
mongoose.connect(process.env.MONGO_URI); // from .env


app.use('/', indexRouter); // http://localhost:6000/
app.use('/api/v1/orders', [passportJWT.isLogin] ,ordersRouter); // http://localhost:6000/api/v1/orders

module.exports = app;

const express = require("express");
const router = express.Router();
// const Product = require("../models/product");

/* GET home page. */
router.get("/", async function (req, res, next) {

  // const product = JSON.parse(req.app.get('product')); //รับข้อมูลจาก app.set ที่ไฟล์ www

  // const productModel = new Product({
  //   product_id: product.id,
  //   name: product.name,
  //   price: product.price,
  // });
  // await productModel.save();

  return res.status(200).json({
    service: "Order Report Service Running...",
  });
});

module.exports = router;

const mongoose = require('mongoose');

const { Schema } = mongoose;

const productSchema = new Schema({
  product_id: Number,
  name:  String, // String is shorthand for {type: String}
  price: Number, // { type: mongoose.Types.Decimal128 }
  created_at: { type: Date, default: Date.now },
},{
    timestamps: false,
    collection: 'products'
});

const product = mongoose.model('Product', productSchema);

module.exports = product;
